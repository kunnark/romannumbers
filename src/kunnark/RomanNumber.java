package kunnark;
import java.util.HashMap;

public class RomanNumber {
	private static HashMap<Integer, String> map = new HashMap<Integer, String>(){
		{
			put(1, "I"); put(5, "V"); 
			put(10, "X"); put(50, "L"); 
			put(100, "C"); put(500, "D");
			put(1000, "M"); 
		}
	};
	
	private static String output;
	
	public static String arabicToRoman(int arabicnumber){
		output = ""; // empty string
		while(arabicnumber > 0 && arabicnumber < 5000){
			int currentnumber = returnFirstNumber(arabicnumber);
			int place = returnPlace(arabicnumber);
				compose(currentnumber, place);
			arabicnumber = returnNextPartOfNumber(arabicnumber);
		}
		return getRomanNumber();
	}// ArabicToRomanNumber
	
	
	private static int returnNextPartOfNumber(int number){
		return (int) ((double) number % Math.pow(10.0, (double) returnPlace(number)));
	}
	
	private static int returnPlace(int number){
		return (int) Math.floor(Math.log10(number));
	}
	
	private static int returnFirstNumber(int number){
		return (int) Character.getNumericValue(String.valueOf(Math.abs((long)number)).charAt(0));
	}// returnFirstNumber
	
	private static int compose(int number, int place){
		if(number >= 1 && number <= 3) out(getStringValue(number, place));
		if(number == 4) {
			out(getStringValue(number-3, place)+getStringValue(number, place));
			number = 0;
		}
		if (number == 5) { 
			out(getStringValue(number, place));
			number = 0; 
		};
		if(number > 5 && number <= 8) { 
			out(getStringValue(number, place));
			number += -4;
		}
		if(number == 9 && place > 0) {
			out(getStringValue(number-8, place)+getStringValue(number, place));
			number = 0;
		}
		else if(number == 9 && place == 0){
			out("IX");
			number = 0;
		}
		if(number == 0) return 0;
		number--;
		return compose(number, place);
	}
	
	private static String getStringValue(int number, int places){
		if (number < 4 ) number = 1;
		if (number >= 4 && number <= 8) number = 5;
		if (number == 9) { 
			places++; number = 1; 
		}
		return map.get((int) ((double) number * Math.pow(10.0, (double) places)));
	}
	
	private static void out(String i){
		StringBuilder out = new StringBuilder();
		out.append(i);
		output += out.toString();
	}
	
	public static String getRomanNumber(){
		return output;
	}
	
}// RomanNumbers
