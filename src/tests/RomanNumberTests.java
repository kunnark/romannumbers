package tests;

import kunnark.RomanNumber;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RomanNumberTests {
	
	@Test
	public void number1Test() {
		int i = 1;
		String result = RomanNumber.arabicToRoman(i);
		assertEquals("I", result);
	}
	
	@Test
	public void number2Test() {
		int i = 2;
		String result = RomanNumber.arabicToRoman(i);
		assertEquals("II", result);
	}
	
	@Test
	public void number3Test() {
		int i = 3;
		String result = RomanNumber.arabicToRoman(i);
		assertEquals("III", result);
	}
	
	@Test
	public void number4Test() {
		int i = 4;
		String result = RomanNumber.arabicToRoman(i);
		assertEquals("IV", result);
	}
	
	@Test
	public void number5Test() {
		int i = 5;
		String result = RomanNumber.arabicToRoman(i);
		assertEquals("V", result);
	}
	
	@Test
	public void number6Test() {
		int i = 6;
		String result = RomanNumber.arabicToRoman(i);
		assertEquals("VI", result);
	}
	
	@Test
	public void number7Test() {
		int i = 7;
		String result = RomanNumber.arabicToRoman(i);
		assertEquals("VII", result);
	}
	@Test
	public void number8Test() {
		int i = 8;
		String result = RomanNumber.arabicToRoman(i);
		assertEquals("VIII", result);
	}
	@Test
	public void number9Test() {
		int i = 9;
		String result = RomanNumber.arabicToRoman(i);
		assertEquals("IX", result);
	}
	@Test
	public void number10Test() {
		int i = 10;
		String result = RomanNumber.arabicToRoman(i);
		assertEquals("X", result);
	}
	@Test
	public void number11Test() {
		int i = 11;
		String result = RomanNumber.arabicToRoman(i);
		assertEquals("XI", result);
	}
	@Test
	public void number20Test() {
		int i = 20;
		String result = RomanNumber.arabicToRoman(i);
		assertEquals("XX", result);
	}
	@Test
	public void number30Test() {
		int i = 30;
		String result = RomanNumber.arabicToRoman(i);
		assertEquals("XXX", result);
	}
	@Test
	public void number40Test() {
		int i = 40;
		String result = RomanNumber.arabicToRoman(i);
		assertEquals("XL", result);
	}
	@Test
	public void number50Test() {
		int i = 50;
		String result = RomanNumber.arabicToRoman(i);
		assertEquals("L", result);
	}
	@Test
	public void number60Test() {
		int i = 60;
		String result = RomanNumber.arabicToRoman(i);
		assertEquals("LX", result);
	}
	@Test
	public void number64Test() {
		int i = 64;
		String result = RomanNumber.arabicToRoman(i);
		assertEquals("LXIV", result);
	}
	
	@Test
	public void number90Test() {
		int i = 90;
		String result = RomanNumber.arabicToRoman(i);
		assertEquals("XC", result);
	}
	
	@Test
	public void number100Test() {
		int i = 100;
		String result = RomanNumber.arabicToRoman(i);
		assertEquals("C", result);
	}
	
	@Test
	public void number198Test() {
		int i = 198;
		String result = RomanNumber.arabicToRoman(i);
		assertEquals("CXCVIII", result);
	}
	
	@Test
	public void number222Test() {
		int i = 222;
		String result = RomanNumber.arabicToRoman(i);
		assertEquals("CCXXII", result);
	}
	
	@Test
	public void number499Test() {
		int i = 499;
		String result = RomanNumber.arabicToRoman(i);
		assertEquals("CDXCIX", result);
	}
	
	@Test
	public void number772Test() {
		int i = 772;
		String result = RomanNumber.arabicToRoman(i);
		assertEquals("DCCLXXII", result);
	}
	
	@Test
	public void number999Test() {
		int i = 999;
		String result = RomanNumber.arabicToRoman(i);
		assertEquals("CMXCIX", result);
	}
	
	@Test
	public void number2999Test() {
		int i = 2999;
		String result = RomanNumber.arabicToRoman(i);
		assertEquals("MMCMXCIX", result);
	}
	
	@Test
	public void number3999Test() {
		int i = 3999;
		String result = RomanNumber.arabicToRoman(i);
		assertEquals("MMMCMXCIX", result);
	}
	
	@Test
	public void number876Test() {
		int i = 876;
		String result = RomanNumber.arabicToRoman(i);
		assertEquals("DCCCLXXVI", result);
	}
	
	@Test
	public void number1903Test() {
		int i = 1903;
		String result = RomanNumber.arabicToRoman(i);
		assertEquals("MCMIII", result);
	}
	


}
